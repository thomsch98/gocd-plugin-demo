# Masking the console log

## Preliminaries - Public repos on bitbucket
Some simple repos to explain the usecase.
A bit over the top (complete dev-env, and a crude
but working plugin) for explaining things in the GoCD context.

### (Almost) Useless Docker as a GoCD material
https://bitbucket.org/thomsch98/dummy-container.git

### The sample plugin
https://bitbucket.org/thomsch98/gocd-plugin-demo.git.
The sample plugin uses a "MaskingJobConsoleLogger" (if configured in the plugin settings) to conceal
Environment variables that have been entered as "Secure".
This is no masking InputStream decorator, but the idea is identical
and the explicit masking of URLs with credentials shows the power of using regular expressions
for the masking ...

### Yet another GoCD Dev - environment (with Docker support in the agent :-))
https://bitbucket.org/thomsch98/gocd-dev.git

## Setting up the use-case
0. Clone the repos of interest
1. Compile the plugin with `mvn clean ; mvn package`
2. Copy the `jar` file from `out` to the `plugin/external` dir of your go-server
3. Setup a pipeline with the configuration settings (material, environment, plugin) from config.txt

## The reason for getting information of "secure variables"
Masking requires the info what to mask, obviously. Values of interest are the
plugin settings and the *secure variables". The following excerpt of the MaskingJobConsoleLogger shows
the usage of **context.environment().secureEnvSpecifier().isSecure(...)** that might be endagered by
the *deprecation* of some of the involved classes/interfaces.

       public MaskingJobConsoleLogger withEnvVars(Map<String, String> map) {
            for (Map.Entry<String, String> e : map.entrySet()) {
                if (context.environment().secureEnvSpecifier().isSecure(e.getKey())) {
                    withMaskString(e.getValue());
                }
            }
            return this;
        }

        @Override
        public void printLine(String line) {
            for (String s : maskValues) {
                line = line.replaceAll(s, mask);
            }
            super.printLine(line);
        }

## Output review

> The "failed" result of the pipeline is of no importance - and I do NOT like to hand out my credentials :-D

### Unmasked
unmasked.log contains the output of a GoCD run that uses only a basic version of the JobConsoleLogger.

#### Obfuscated environment variable names in [go] messages
    16:20:07.787 [go] setting environment variable 'demo_******' to value '********'
If the variable name contains the secure value, that part of the variable name is obfuscated by the "[go]" logging, too.
That doesn't happen "in the plugin" with `console.printEnvironment(envVars);`

#### Secure variables are displayed by `console.printline();`
    16:20:15.010 [docker, login, -p, regpwd, -u, regusr, registry.demo.io]
This is to be expected and the reason for some "masking" approach.

### Masked
maske.log contains the output of the identical pipeline, except the "Masking" setting of the plugin.

#### Obfuscated environment variable names in [go] messages
Nothing changes. That "problem" is located in the server/agent and not in the plugin.

#### Secure variables are concealed correctly when using `console.printline();`
    16:22:31.786 [docker, login, -p, ********, -u, regusr, registry.demo.io]
That is what we wanted to achieve ...

## Small surprise at the end ...
Compare "Unmasked"

    16:20:23.003 Step 4 : RUN wget --no-check-certificate https://${BBCREDS}@bitbucket.org/haufegroup/dummy-container/raw/300e2c1216d62b1168716ac4dd6ea5bad069833c/Dockerfile
    16:20:23.004 [91m--2016-05-16 16:20:22--  https://abc:*password*@bitbucket.org/haufegroup/dummy-container/raw/300e2c1216d62b1168716ac4dd6ea5bad069833c/Dockerfile

With "Masked"

    16:22:50.619 Step 4 : RUN wget --no-check-certificate https://********@bitbucket.org/haufegroup/********-container/raw/300e2c1216d62b1168716ac4dd6ea5bad069833c/Dockerfile
    16:22:50.625 [91m--2016-05-16 16:22:49--  https://********@bitbucket.org/haufegroup/********-container/raw/300e2c1216d62b1168716ac4dd6ea5bad069833c/Dockerfile

Neither the string `${BBCREDS}` nor `*password*` was inserted/modified by the plugin.
Maybe some magic of GoCD??
