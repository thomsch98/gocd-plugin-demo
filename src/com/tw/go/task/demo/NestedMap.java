package com.tw.go.task.demo;

import java.util.Map;

/**
 * Created by thomassc on 16.05.16.
 */
class NestedMap {
    Map map;

    public NestedMap(Map map) {
        this.map = map;
    }

    public NestedMap getNested(String key) {
        return new NestedMap((Map) map.get(key));
    }

    public Map<String, Object> getMap(String key) {
        return (Map<String, Object>) map.get(key);
    }

    public Map<String, String> getStringMap(String key) {
        return (Map<String, String>) map.get(key);
    }

    public String getString(String key) {
        return (String) map.get(key);
    }

    public String getValueString(String key) {
        return (String) getMap(key).get("value");
    }

    public boolean getValueBool(String key) {
        return (boolean) getMap(key).get("value");
    }

    public int getValueInt(String key) {
        return (int) (double) getMap(key).get("value");
    }
}
