package com.tw.go.task.demo;

import com.thoughtworks.go.plugin.api.task.JobConsoleLogger;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by thomassc on 16.05.16.
 */
class MaskingJobConsoleLogger extends JobConsoleLogger {
    Set<String> maskValues = new HashSet<>();
    String mask = "********";

    public MaskingJobConsoleLogger withMask(String s) {
        mask = s;
        return this;
    }

    public MaskingJobConsoleLogger withMaskRegex(String s) {
        maskValues.add(s);
        return this;
    }

    public MaskingJobConsoleLogger withMaskString(String s) {
        maskValues.add("\\b" + Pattern.quote(s) + "\\b");
        return this;
    }

    public MaskingJobConsoleLogger withEnvVars(Map<String, String> map) {
        for (Map.Entry<String, String> e : map.entrySet()) {
            if (context.environment().secureEnvSpecifier().isSecure(e.getKey())) {
                withMaskString(e.getValue());
            }
        }
        return this;
    }

    @Override
    public void printLine(String line) {
        for (String s : maskValues) {
            line = line.replaceAll(s, mask);
        }
        super.printLine(line);
    }
}
