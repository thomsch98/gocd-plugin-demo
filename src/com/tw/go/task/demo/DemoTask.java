
package com.tw.go.task.demo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thoughtworks.go.plugin.api.AbstractGoPlugin;
import com.thoughtworks.go.plugin.api.GoPluginIdentifier;
import com.thoughtworks.go.plugin.api.annotation.Extension;
import com.thoughtworks.go.plugin.api.request.GoPluginApiRequest;
import com.thoughtworks.go.plugin.api.response.DefaultGoPluginApiResponse;
import com.thoughtworks.go.plugin.api.response.GoPluginApiResponse;
import com.thoughtworks.go.plugin.api.task.JobConsoleLogger;
import org.apache.commons.io.IOUtils;

import java.util.*;

@Extension
public class DemoTask extends AbstractGoPlugin {

    public static final String REGEX_URL_CREDS = "(?<=://)[^:@]+(:[^@]+)?(?=@)";
    Gson gson = new GsonBuilder().create();

    @Override
    public GoPluginApiResponse handle(GoPluginApiRequest request) {
        try {
            switch (request.requestName()) {
                case "configuration":
                case "go.plugin-settings.get-configuration":
                    return handleGetConfigRequest(request);
                case "validate":
                case "go.plugin-settings.validate-configuration":
                    return handleValidation(request);
                case "execute":
                    return handleTaskExecution(request);
                case "view":
                case "go.plugin-settings.get-view":
                    return handleTaskView(request);
                default:
                    return DefaultGoPluginApiResponse.badRequest(String.format("Invalid request name %s", request.requestName()));
            }
        } catch (Throwable e) {
            return DefaultGoPluginApiResponse.error(e.getMessage());
        }
    }

    private String getId() {
        return "Demo";
    }

    private GoPluginApiResponse handleTaskView(GoPluginApiRequest request) {
        HashMap result = new HashMap();
        result.put("displayValue", getId());
        try {
            result.put("template", IOUtils.toString(getClass().getResourceAsStream("/views/task.template.html"), "UTF-8"));
            return DefaultGoPluginApiResponse.success(gson.toJson(result));
        } catch (Exception e) {
            String errorMessage = "Failed to find template: " + e.getMessage();
            result.put("exception", errorMessage);
            return DefaultGoPluginApiResponse.error(gson.toJson(result));
        }
    }

    private GoPluginApiResponse handleValidation(GoPluginApiRequest request) {
        HashMap config = new HashMap();
        return execSuccess(gson.toJson(config));
    }

    private GoPluginApiResponse handleTaskExecution(GoPluginApiRequest request) {
        NestedMap nm = new NestedMap(gson.fromJson(request.requestBody(), Map.class));
        NestedMap config = nm.getNested("config");
        Map<String, String> envVars = nm.getNested("context").getStringMap("environmentVariables");
        String workingDir = nm.getNested("context").getString("workingDirectory");
        String masking = config.getValueString("Masking");
        JobConsoleLogger console = "true".equals(masking)
                ? new MaskingJobConsoleLogger().withEnvVars(envVars).withMaskRegex(REGEX_URL_CREDS)
                : JobConsoleLogger.getConsoleLogger();
        console.printEnvironment(envVars);
        try {
            doExec(console, Arrays.asList(
                    "docker",
                    "login",
                    "-p", envVars.get("REG_PWD"),
                    "-u", envVars.get("REG_USR"),
                    envVars.get("REG_URL")), envVars, true);
            doExec(console, Arrays.asList(
                    "docker",
                    "build",
                    "-t",
                    envVars.get("GO_REVISION"),
                    "--build-arg",
                    "BBCREDS=" + envVars.get("BBCREDS"),
                    "--no-cache", workingDir), envVars, false);
            return execSuccess("Everything ok");
        } catch (Exception e) {
            return execFailure("Something went wrong: " + e.getMessage());
        }
    }

    private void doExec(JobConsoleLogger console, List<String> command, Map<String, String> envVars, boolean ignoreErrors) throws Exception {
        synchronized (getId()) {
            ProcessBuilder processBuilder = new ProcessBuilder(command);
            processBuilder.environment().putAll(envVars);
            Process process = processBuilder.start();
            int ret = process.waitFor();
            console.printLine(processBuilder.command().toString());

            if (false) {

                // this doesn't show any output ...
                console.readOutputOf(process.getInputStream());
                console.readErrorOf(process.getErrorStream());

            } else {

                List<String> out = Utils.readLines(process.getInputStream());
                List<String> err = Utils.readLines(process.getErrorStream());

                for (String s : out) {
                    console.printLine(s);
                }

                if (ret != 0) {
                    for (String s : err) {
                        console.printLine(s);
                    }
                }
            }

            Utils.closeQuietly(process.getInputStream());
            Utils.closeQuietly(process.getErrorStream());
            Utils.closeQuietly(process.getOutputStream());

            process.destroy();

            if (ret != 0) {
                if (!ignoreErrors) {
                    throw new RuntimeException("Process returned " + ret);
                }
                console.printLine("Ignoring exit code " + ret);
            }
        }
    }

    private GoPluginApiResponse handleGetConfigRequest(GoPluginApiRequest request) {

        HashMap config = new HashMap();
        config.put("Masking", prop("off", false));
        config.put("Demo", prop("", false));
        config.put("Demo1", prop("", false));
        return DefaultGoPluginApiResponse.success(gson.toJson(config));
    }

    private Map prop(Object defaultValue) {
        return prop(defaultValue, true);
    }

    private Map prop(Object defaultValue, boolean required) {
        return prop(defaultValue, required, false);
    }

    private Map prop(Object defaultValue, boolean required, boolean secure) {
        HashMap config = new HashMap();
        config.put("default-value", defaultValue);
        config.put("required", required);
        config.put("secure", secure);
        return config;
    }

    private static GoPluginApiResponse execSuccess(String message) {
        return DefaultGoPluginApiResponse.success(String.format("{ \"success\": %s, \"message\": \"%s\" }", true, message));
    }

    private static GoPluginApiResponse execFailure(String message) {
        return DefaultGoPluginApiResponse.success(String.format("{ \"success\": %s, \"message\": \"%s\" }", false, message));
    }

    @Override
    public GoPluginIdentifier pluginIdentifier() {
        return new GoPluginIdentifier("task", Arrays.asList("1.0"));
    }
}
