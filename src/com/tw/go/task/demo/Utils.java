package com.tw.go.task.demo;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomassc on 16.05.16.
 * Copied from various sources to keep example small
 */
public class Utils {
    public static List<String> readLines(InputStream s) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(s));
        ArrayList<String> lines = new ArrayList<>();
        String line;
        while ((line = br.readLine()) != null) {
            lines.add(line);
        }
        return lines;
    }

    public static void closeQuietly(InputStream input) {
        closeQuietly((Closeable) input);
    }

    public static void closeQuietly(OutputStream output) {
        closeQuietly((Closeable) output);
    }

    public static void closeQuietly(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException var2) {
            ;
        }

    }
}
